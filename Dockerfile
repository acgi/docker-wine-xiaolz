FROM ubuntu:eoan

ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive

ENV LC_ALL zh_CN.UTF-8
ENV LANG zh_CN.UTF-8
ENV LANGUAGE zh_CN.UTF-8
ENV TZ Asia/Shanghai

RUN dpkg --add-architecture i386

# RUN sed -i s@/archive.ubuntu.com/@/mirrors.aliyun.com/@g /etc/apt/sources.list
RUN apt-get clean
RUN apt-get update && apt-get -y install xvfb x11vnc xdotool wget tar supervisor net-tools fluxbox gnupg2 language-pack-zh-hans vim unzip autocutsel iputils-ping
RUN apt-get install -y nginx

COPY ./docker-root /
COPY ./docker-plus /root/

RUN \
    # wine regedit.exe /s /font/zh.reg && \
    mkdir -p /home/user/.wine/drive_c/windows/Fonts && \
    unzip /font/simsun.zip -d /home/user/.wine/drive_c/windows/Fonts && \
    mkdir /root/.fonts && \
    ln -s /home/user/.wine/drive_c/windows/Fonts/simsun.ttc /root/.fonts/ 
    # fc-cache -v

RUN wget -O - https://dl.winehq.org/wine-builds/winehq.key | apt-key add -
RUN echo 'deb https://dl.winehq.org/wine-builds/ubuntu/ eoan main' |tee /etc/apt/sources.list.d/winehq.list
RUN apt-get update && apt-get -y install winehq-stable winetricks

RUN mkdir -p /opt/wine-stable/share/wine/mono && tar -xzf /root/wine-mono-bin-4.9.4.tar.gz -C /root/ && mv /root/wine-mono-4.9.4 /opt/wine-stable/share/wine/mono/
RUN mkdir -p /opt/wine-stable/share/wine/gecko && cp /root/wine-gecko-2.47.1-x86.msi /opt/wine-stable/share/wine/gecko/wine-gecko-2.47.1-x86.msi && cp /root/wine-gecko-2.47.1-x86_64.msi /opt/wine-stable/share/wine/gecko/wine-gecko-2.47.1-x86_64.msi 
RUN apt-get -y full-upgrade && apt-get clean
ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ENV WINEPREFIX /root/prefix32
ENV WINEARCH win32
ENV DISPLAY :0

WORKDIR /root/
RUN tar -xzf /root/noVNC-1.2.0.tar.gz -C /root/ && mv /root/noVNC-1.2.0 /root/novnc && ln -s /root/novnc/vnc_lite.html /root/novnc/index.html
RUN mkdir -p /root/novnc/utils && tar -xzf /root/websockify-0.9.0.tar.gz -C /root/ && mv /root/websockify-0.9.0 /root/novnc/utils/websockify

RUN fc-cache -v

EXPOSE 8080

CMD ["/usr/bin/supervisord"]